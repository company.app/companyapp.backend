package com.agrzybow.companyapp.backend.inboxmanagement.model.mapper;

import com.agrzybow.companyapp.backend.general.model.mapper.UserEntityDTOMapper;
import com.agrzybow.companyapp.backend.inboxmanagement.model.InboxItemEntity;
import com.agrzybow.companyapp.backend.inboxmanagement.model.dto.InboxItemDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = UserEntityDTOMapper.class)
public interface InboxItemEntityDTOMapper {
    List<InboxItemEntity> dtosToEntities(List<InboxItemDTO> formDTOs);
    List<InboxItemDTO> entitiesToDtos(List<InboxItemEntity> formEntities);
    InboxItemDTO entityToDto(InboxItemEntity inboxItemEntity);
}
