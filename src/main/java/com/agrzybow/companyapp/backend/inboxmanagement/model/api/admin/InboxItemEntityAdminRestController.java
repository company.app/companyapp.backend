package com.agrzybow.companyapp.backend.inboxmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import com.agrzybow.companyapp.backend.general.model.repository.UserEntityRepository;
import com.agrzybow.companyapp.backend.inboxmanagement.model.InboxItemEntity;
import com.agrzybow.companyapp.backend.inboxmanagement.model.repository.InboxItemEntityRepository;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/inbox-item")
public class InboxItemEntityAdminRestController implements AdminRestControllerInterface<InboxItemEntity, Integer> {
    private InboxItemEntityRepository inboxItemEntityRepository;
    private UserEntityRepository userEntityRepository;

    @Autowired
    public InboxItemEntityAdminRestController(InboxItemEntityRepository inboxItemEntityRepository, UserEntityRepository userEntityRepository) {
        this.inboxItemEntityRepository = inboxItemEntityRepository;
        this.userEntityRepository = userEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<InboxItemEntity> getAllEntities() {
        return inboxItemEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public InboxItemEntity getEntityById(@PathVariable Integer id) {
        return inboxItemEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<InboxItemEntity> createEntity(@RequestBody List<InboxItemEntity> newEntity) {
        newEntity.forEach(inboxItemEntity -> {
            userEntityRepository.findById(inboxItemEntity.getReceiver().getUsername()).ifPresent(receiver -> {
                try {
                    FirebaseMessaging.getInstance().send(Message.builder()
                            .putData("type", "InboxItem")
                            .setToken(receiver.getToken())
                            .build());
                } catch (FirebaseMessagingException e) {
                    e.printStackTrace();
                }
            });
        });
        return inboxItemEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public InboxItemEntity editEntity(@RequestBody InboxItemEntity editedEntity) {
        return inboxItemEntityRepository.save(editedEntity);
    }
}
