package com.agrzybow.companyapp.backend.quizmanagement.model;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import com.agrzybow.companyapp.backend.general.util.UserEntitySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "QuizAnswer")
public class QuizAnswerEntity {
    public static final String ID_FIELD_NAME = "id";
    public static final String ANSWER_TEXT_FIELD_NAME = "answerText";
    public static final String IS_CORRECT_FIELD_NAME = "isCorrect";
    public static final String QUESTION_ID_FIELD_NAME = "question";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @Size(min = 1)
    @Column(name = ANSWER_TEXT_FIELD_NAME, nullable = false)
    private String answerText;

    @NotNull
    @Column(name = IS_CORRECT_FIELD_NAME, nullable = false)
    private Boolean isCorrect;

    @NotNull
    @ManyToOne
    @JoinColumn(name = QUESTION_ID_FIELD_NAME, nullable = false)
    private QuizQuestionEntity question;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFY_BY_FIELD_NAME)
    @JsonSerialize(using = UserEntitySerializer.class)
    private UserEntity modifyBy;
}
