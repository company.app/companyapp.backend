package com.agrzybow.companyapp.backend.quizmanagement.model;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import com.agrzybow.companyapp.backend.general.util.UserEntitySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "QuizUserAnswer")
public class QuizUserAnswerEntity {
    public static final String ID_FIELD_NAME = "id";
    public static final String ANSWER_ID_FIELD_NAME = "answer";
    public static final String QUESTION_ID_FIELD_NAME = "question";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = ANSWER_ID_FIELD_NAME, nullable = false)
    private QuizAnswerEntity answer;

    @NotNull
    @ManyToOne
    @JoinColumn(name = QUESTION_ID_FIELD_NAME, nullable = false)
    private QuizQuestionEntity question;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFY_BY_FIELD_NAME)
    @JsonSerialize(using = UserEntitySerializer.class)
    private UserEntity modifyBy;
}
