package com.agrzybow.companyapp.backend.quizmanagement.model.mapper;

import com.agrzybow.companyapp.backend.quizmanagement.model.QuizCategoryEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.dto.QuizCategoryDTO;
import org.mapstruct.Mapper;

@Mapper
public interface QuizCategoryEntityDTOMapper {
    QuizCategoryEntity dtoToEntity(QuizCategoryDTO quizCategoryDTO);
    QuizCategoryDTO entityToDto(QuizCategoryEntity quizCategoryEntity);
}
