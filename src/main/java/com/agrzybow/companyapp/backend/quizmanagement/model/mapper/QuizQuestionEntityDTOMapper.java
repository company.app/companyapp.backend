package com.agrzybow.companyapp.backend.quizmanagement.model.mapper;

import com.agrzybow.companyapp.backend.quizmanagement.model.QuizQuestionEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.dto.QuizQuestionDTO;
import org.mapstruct.Mapper;

@Mapper(uses = QuizCategoryEntityDTOMapper.class)
public interface QuizQuestionEntityDTOMapper {
    QuizQuestionEntity dtoToEntity(QuizQuestionDTO quizQuestionDTO);
    QuizQuestionDTO entityToDto(QuizQuestionEntity quizQuestionEntity);
}
