package com.agrzybow.companyapp.backend.quizmanagement.model.mapper;

import com.agrzybow.companyapp.backend.general.model.mapper.UserEntityDTOMapper;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizUserAnswerEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.dto.QuizUserAnswerDTO;
import org.mapstruct.Mapper;

@Mapper(uses = {QuizAnswerEntityDTOMapper.class, QuizQuestionEntityDTOMapper.class, UserEntityDTOMapper.class})
public interface QuizUserAnswerEntityDTOMapper {
    QuizUserAnswerEntity dtoToEntity(QuizUserAnswerDTO quizUserAnswerDTO);
    QuizUserAnswerDTO entityToDto(QuizUserAnswerEntity quizUserAnswerEntity);
}
