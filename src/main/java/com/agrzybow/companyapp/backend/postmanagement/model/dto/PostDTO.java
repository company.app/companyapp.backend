package com.agrzybow.companyapp.backend.postmanagement.model.dto;

import lombok.Data;

@Data
public class PostDTO {
    private Integer id;
    private String title;
    private String postText;
    private Long modifyDate;
}
