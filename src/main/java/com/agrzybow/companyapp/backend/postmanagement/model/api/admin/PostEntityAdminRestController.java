package com.agrzybow.companyapp.backend.postmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import com.agrzybow.companyapp.backend.postmanagement.model.PostEntity;
import com.agrzybow.companyapp.backend.postmanagement.model.repository.PostEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/post")
public class PostEntityAdminRestController implements AdminRestControllerInterface<PostEntity, Integer> {
    private PostEntityRepository postEntityRepository;

    @Autowired
    public PostEntityAdminRestController(PostEntityRepository postEntityRepository) {
        this.postEntityRepository = postEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<PostEntity> getAllEntities() {
        return postEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public PostEntity getEntityById(@PathVariable Integer id) {
        return postEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<PostEntity> createEntity(@RequestBody List<PostEntity> newEntity) {
        return postEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public PostEntity editEntity(@RequestBody PostEntity editedEntity) {
        return postEntityRepository.save(editedEntity);
    }
}
