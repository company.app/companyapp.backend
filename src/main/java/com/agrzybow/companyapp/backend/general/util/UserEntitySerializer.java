package com.agrzybow.companyapp.backend.general.util;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class UserEntitySerializer extends JsonSerializer<UserEntity> {
    @Override
    public void serialize(UserEntity userEntity, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField(UserEntity.USERNAME_FIELD_NAME, userEntity.getUsername());
        jsonGenerator.writeStringField(UserEntity.NAME_FIELD_NAME, userEntity.getName());
        jsonGenerator.writeStringField(UserEntity.EMAIL_FIELD_NAME, userEntity.getEmail());
        jsonGenerator.writeStringField(UserEntity.PHONE_NUMBER_FIELD_NAME, userEntity.getPhoneNumber());
        jsonGenerator.writeEndObject();
    }
}
