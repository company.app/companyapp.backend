package com.agrzybow.companyapp.backend.general.model;

import com.agrzybow.companyapp.backend.general.util.UserEntitySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "User")
public class UserEntity {
    public static final String USERNAME_FIELD_NAME = "username";
    public static final String PASSWORD_FIELD_NAME = "password";
    public static final String PASSWORD_EXPIRED_AT_FIELD_NAME = "passwordExpiredAt";
    public static final String NAME_FIELD_NAME = "name";
    public static final String EMAIL_FIELD_NAME = "email";
    public static final String PERMISSION_FIELD_NAME = "userPermission";
    public static final String GROUP_ID_FIELD_NAME = "userGroup";
    public static final String TOKEN_FIELD_NAME = "token";
    public static final String PHONE_NUMBER_FIELD_NAME = "phoneNumber";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @NotNull
    @Size(min = 1)
    @Id
    @Column(name = USERNAME_FIELD_NAME, nullable = false, length = 63)
    private String username;

    @NotNull
    @Size(min = 1)
    @Column(name = PASSWORD_FIELD_NAME, nullable = false)
    private String password;

    @Column(name = TOKEN_FIELD_NAME)
    private String token;

    @NotNull
    @Min(1)
    @Column(name = PASSWORD_EXPIRED_AT_FIELD_NAME, nullable = false)
    private Long passwordExpiredAt;

    @NotNull
    @Size(min = 1)
    @Column(name = NAME_FIELD_NAME, nullable = false)
    private String name;

    @NotNull
    @Email
    @Size(min = 1)
    @Column(name = EMAIL_FIELD_NAME)
    private String email;

    @NotNull
    @Size(min = 1)
    @Column(name = PHONE_NUMBER_FIELD_NAME, nullable = false)
    private String phoneNumber;

    @NotNull
    @ManyToOne
    @JoinColumn(name = GROUP_ID_FIELD_NAME, nullable = false)
    private UserGroupEntity userGroup;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = PERMISSION_FIELD_NAME, nullable = false)
    private UserPermissionEntity userPermission;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFY_BY_FIELD_NAME)
    @JsonSerialize(using = UserEntitySerializer.class)
    private UserEntity modifyBy;
}
