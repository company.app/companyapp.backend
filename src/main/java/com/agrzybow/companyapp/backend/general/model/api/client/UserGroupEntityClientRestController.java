package com.agrzybow.companyapp.backend.general.model.api.client;

import com.agrzybow.companyapp.backend.general.model.dto.UserGroupDTO;
import com.agrzybow.companyapp.backend.general.model.mapper.UserGroupEntityDTOMapper;
import com.agrzybow.companyapp.backend.general.model.repository.UserGroupEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/client/user-group")
public class UserGroupEntityClientRestController {
    private UserGroupEntityRepository userGroupEntityRepository;
    private UserGroupEntityDTOMapper mapper;

    @Autowired
    public UserGroupEntityClientRestController(UserGroupEntityRepository userGroupEntityRepository, UserGroupEntityDTOMapper mapper) {
        this.userGroupEntityRepository = userGroupEntityRepository;
        this.mapper = mapper;
    }

    @GetMapping("/display")
    public List<UserGroupDTO> getAllEntities(@RequestParam(required = false) Long newerThan) {
        if(newerThan != null) {
            return mapper.entitiesToDtos(userGroupEntityRepository.findByModifyDateGreaterThan(newerThan).orElse(new ArrayList<>()));
        }
        return mapper.entitiesToDtos(userGroupEntityRepository.findAll());
    }
}
