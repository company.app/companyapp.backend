package com.agrzybow.companyapp.backend.general.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDTO {
    private String username;
    private String name;
    private String email;
    private String phoneNumber;
    private UserGroupDTO userGroup;
    private Long modifyDate;
}
