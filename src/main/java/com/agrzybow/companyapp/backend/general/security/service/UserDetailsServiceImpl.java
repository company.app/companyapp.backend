package com.agrzybow.companyapp.backend.general.security.service;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import com.agrzybow.companyapp.backend.general.model.UserPermissionEntity;
import com.agrzybow.companyapp.backend.general.model.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private UserEntityRepository userEntityRepository;

    @Autowired
    public UserDetailsServiceImpl(UserEntityRepository userEntityRepository) {
        this.userEntityRepository = userEntityRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity userEntity = userEntityRepository.findById(s)
                .orElseThrow(() -> new UsernameNotFoundException(s));
        UserPermissionEntity permissions = userEntity.getUserPermission();

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        if(permissions.getConsoleAdd()) grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADD"));
        if(permissions.getConsoleEdit()) grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_EDIT"));
        if(permissions.getConsoleDisplay()) grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_DISPLAY"));

        return new User(userEntity.getUsername(), userEntity.getPassword(), grantedAuthorities);
    }
}
