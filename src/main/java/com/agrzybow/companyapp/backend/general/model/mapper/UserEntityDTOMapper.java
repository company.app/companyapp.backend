package com.agrzybow.companyapp.backend.general.model.mapper;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import com.agrzybow.companyapp.backend.general.model.dto.UserDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = UserGroupEntityDTOMapper.class)
public interface UserEntityDTOMapper {
    UserEntity dtoToEntity(UserDTO userDTO);
    UserDTO entityToDto(UserEntity userEntity);
    List<UserEntity> dtosToEntities(List<UserDTO> userDTOs);
    List<UserDTO> entitiesToDtos(List<UserEntity> userEntities);
}
