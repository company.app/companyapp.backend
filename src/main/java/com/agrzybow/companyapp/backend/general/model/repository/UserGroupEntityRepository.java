package com.agrzybow.companyapp.backend.general.model.repository;

import com.agrzybow.companyapp.backend.general.model.UserGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserGroupEntityRepository extends JpaRepository<UserGroupEntity, Integer> {
    Optional<List<UserGroupEntity>> findByModifyDateGreaterThan(Long newerThan);
}
