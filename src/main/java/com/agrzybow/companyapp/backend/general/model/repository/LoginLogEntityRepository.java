package com.agrzybow.companyapp.backend.general.model.repository;

import com.agrzybow.companyapp.backend.general.model.LoginLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginLogEntityRepository extends JpaRepository<LoginLogEntity, Integer> {
}
