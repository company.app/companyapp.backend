package com.agrzybow.companyapp.backend.general.model.api.admin;

import java.util.Collections;
import java.util.List;

public interface AdminRestControllerInterface<T, X> {
    List<T> getAllEntities();
    T getEntityById(X id);
    List<T> createEntity(List<T> newEntities);
    T editEntity(T editedEntity);

    default T createEntity(T newEntity) {
        return createEntity(Collections.singletonList(newEntity)).get(0);
    }
}
