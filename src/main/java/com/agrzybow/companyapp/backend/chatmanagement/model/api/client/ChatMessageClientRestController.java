package com.agrzybow.companyapp.backend.chatmanagement.model.api.client;

import com.agrzybow.companyapp.backend.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.backend.chatmanagement.model.dto.ChatMessageDTO;
import com.agrzybow.companyapp.backend.chatmanagement.model.mapper.ChatMessageEntityDTOMapper;
import com.agrzybow.companyapp.backend.chatmanagement.model.repository.ChatMessageEntityRepository;
import com.agrzybow.companyapp.backend.general.model.repository.UserEntityRepository;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/client/chat-message")
public class ChatMessageClientRestController {
    private ChatMessageEntityRepository chatMessageEntityRepository;
    private UserEntityRepository userEntityRepository;
    private ChatMessageEntityDTOMapper mapper;

    @Autowired
    public ChatMessageClientRestController(ChatMessageEntityRepository chatMessageEntityRepository, UserEntityRepository userEntityRepository, ChatMessageEntityDTOMapper mapper) {
        this.chatMessageEntityRepository = chatMessageEntityRepository;
        this.userEntityRepository = userEntityRepository;
        this.mapper = mapper;
    }

    @PreAuthorize("#username == authentication.name")
    @GetMapping("/display/{username}")
    public List<ChatMessageDTO> getAllMessagesForUser(@PathVariable String username, @RequestParam(required = false) Long newerThan) {
        UserEntity userEntity = UserEntity.builder().username(username).build();
        List<ChatMessageEntity> result = new ArrayList<>();
        if (newerThan != null) {
            result.addAll(chatMessageEntityRepository.findByModifyByAndModifyDateGreaterThan(userEntity, newerThan));
            result.addAll(chatMessageEntityRepository.findByUserEntityReceiverAndModifyDateGreaterThan(userEntity, newerThan));
        } else {
            result.addAll(chatMessageEntityRepository.findByModifyBy(userEntity));
            result.addAll(chatMessageEntityRepository.findByUserEntityReceiver(userEntity));
        }
        return mapper.entitiesToDtos(result);
    }

    @PreAuthorize("#username == authentication.name")
    @PostMapping("/add/{username}")
    public ChatMessageDTO saveEntity(@PathVariable String username, @RequestBody ChatMessageDTO chatMessageDTO) {
        ChatMessageEntity chatMessageEntity = chatMessageEntityRepository.save(mapper.dtoToEntity(chatMessageDTO));
        try {
            UserEntity receiver = userEntityRepository.findById(chatMessageEntity.getUserEntityReceiver().getUsername()).orElse(null);
            if (receiver != null) {
                FirebaseMessaging.getInstance().send(Message.builder()
                        .putData("type", "ChatMessage")
                        .putData("id", chatMessageEntity.getId().toString())
                        .putData("title", chatMessageEntity.getModifyBy().getName())
                        .putData("content", chatMessageEntity.getMessageText())
                        .putData("tag", chatMessageEntity.getModifyBy().getUsername())
                        .setToken(receiver.getToken())
                        .build());
            }
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
        return mapper.entityToDto(chatMessageEntity);
    }
}
