package com.agrzybow.companyapp.backend.chatmanagement.model.mapper;

import com.agrzybow.companyapp.backend.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.backend.general.model.mapper.UserEntityDTOMapper;
import com.agrzybow.companyapp.backend.chatmanagement.model.dto.ChatMessageDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = UserEntityDTOMapper.class)
public interface ChatMessageEntityDTOMapper {
    ChatMessageEntity dtoToEntity(ChatMessageDTO chatMessageDTO);
    ChatMessageDTO entityToDto(ChatMessageEntity chatMessageEntity);
    List<ChatMessageEntity> dtosToEntities(List<ChatMessageDTO> chatMessageDTOs);
    List<ChatMessageDTO> entitiesToDtos(List<ChatMessageEntity> chatMessageEntities);
}
