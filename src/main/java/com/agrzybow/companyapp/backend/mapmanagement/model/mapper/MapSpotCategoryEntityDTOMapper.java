package com.agrzybow.companyapp.backend.mapmanagement.model.mapper;

import com.agrzybow.companyapp.backend.mapmanagement.model.MapSpotCategoryEntity;
import com.agrzybow.companyapp.backend.mapmanagement.model.dto.MapSpotCategoryDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface MapSpotCategoryEntityDTOMapper {
    MapSpotCategoryDTO entityToDto(MapSpotCategoryEntity mapSpotCategoryEntity);
    MapSpotCategoryEntity dtoToEntity(MapSpotCategoryDTO mapSpotCategoryDTO);
    List<MapSpotCategoryEntity> dtosToEntities(List<MapSpotCategoryDTO> mapSpotCategoryDTOs);
    List<MapSpotCategoryDTO> entitiesToDtos(List<MapSpotCategoryEntity> mapSpotCategoryEntities);
}
