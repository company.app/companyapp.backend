package com.agrzybow.companyapp.backend.mapmanagement.model.api.client;

import com.agrzybow.companyapp.backend.mapmanagement.model.dto.MapSpotCategoryDTO;
import com.agrzybow.companyapp.backend.mapmanagement.model.mapper.MapSpotCategoryEntityDTOMapper;
import com.agrzybow.companyapp.backend.mapmanagement.model.repository.MapSpotCategoryEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/client/map-spot-category")
public class MapSpotCategoryEntityClientRestController {
    private MapSpotCategoryEntityRepository mapSpotCategoryEntityRepository;
    private MapSpotCategoryEntityDTOMapper mapper;

    @Autowired
    public MapSpotCategoryEntityClientRestController(MapSpotCategoryEntityRepository mapSpotCategoryEntityRepository, MapSpotCategoryEntityDTOMapper mapper) {
        this.mapSpotCategoryEntityRepository = mapSpotCategoryEntityRepository;
        this.mapper = mapper;
    }

    @GetMapping("/display")
    public List<MapSpotCategoryDTO> getAllEntities(@RequestParam(required = false) Long newerThan) {
        if(newerThan != null) {
            return mapper.entitiesToDtos(mapSpotCategoryEntityRepository.findByModifyDateGreaterThan(newerThan));
        }
        return mapper.entitiesToDtos(mapSpotCategoryEntityRepository.findAll());
    }
}
