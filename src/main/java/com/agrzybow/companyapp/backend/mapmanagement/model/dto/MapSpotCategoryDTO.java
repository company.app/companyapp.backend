package com.agrzybow.companyapp.backend.mapmanagement.model.dto;

import lombok.Data;

@Data
public class MapSpotCategoryDTO {
    private Integer id;
    private String name;
    private String description;
    private byte[] image;
    private Long modifyDate;
}
