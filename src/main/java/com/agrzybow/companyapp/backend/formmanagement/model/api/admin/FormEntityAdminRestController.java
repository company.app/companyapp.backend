package com.agrzybow.companyapp.backend.formmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.formmanagement.model.FormEntity;
import com.agrzybow.companyapp.backend.formmanagement.model.repository.FormEntityRepository;
import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/form")
public class FormEntityAdminRestController implements AdminRestControllerInterface<FormEntity, Integer> {
    private FormEntityRepository formEntityRepository;

    @Autowired
    public FormEntityAdminRestController(FormEntityRepository formEntityRepository) {
        this.formEntityRepository = formEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<FormEntity> getAllEntities() {
        return formEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public FormEntity getEntityById(@PathVariable Integer id) {
        return formEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<FormEntity> createEntity(@RequestBody List<FormEntity> newEntity) {
        return formEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public FormEntity editEntity(@RequestBody FormEntity editedEntity) {
        return formEntityRepository.save(editedEntity);
    }
}
