package com.agrzybow.companyapp.backend.formmanagement.model.dto;

import lombok.Data;

@Data
public class FormDTO {
    private Integer id;
    private String name;
    private String url;
    private Long modifyDate;
}
