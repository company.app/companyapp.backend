package com.agrzybow.companyapp.backend.formmanagement.model.repository;

import com.agrzybow.companyapp.backend.formmanagement.model.FormEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FormEntityRepository extends JpaRepository<FormEntity, Integer> {
    Optional<List<FormEntity>> findByModifyDateGreaterThan(Long modifyDate);
}
