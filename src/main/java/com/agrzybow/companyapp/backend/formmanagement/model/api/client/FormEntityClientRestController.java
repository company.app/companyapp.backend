package com.agrzybow.companyapp.backend.formmanagement.model.api.client;

import com.agrzybow.companyapp.backend.formmanagement.model.dto.FormDTO;
import com.agrzybow.companyapp.backend.formmanagement.model.mapper.FormEntityDTOMapper;
import com.agrzybow.companyapp.backend.formmanagement.model.repository.FormEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/client/form")
public class FormEntityClientRestController {
    private FormEntityRepository formEntityRepository;
    private FormEntityDTOMapper mapper;

    @Autowired
    public FormEntityClientRestController(FormEntityRepository formEntityRepository, FormEntityDTOMapper mapper) {
        this.formEntityRepository = formEntityRepository;
        this.mapper = mapper;
    }

    @GetMapping("/display")
    public List<FormDTO> getAllEntities(@RequestParam(required = false) Long newerThan) {
        if(newerThan != null) {
            return mapper.entitiesToDtos(formEntityRepository.findByModifyDateGreaterThan(newerThan).orElse(new ArrayList<>()));
        }
        return mapper.entitiesToDtos(formEntityRepository.findAll());
    }

    @GetMapping("/display/{id}")
    public FormDTO getEntityById(@PathVariable Integer id) {
        return mapper.entityToDto(formEntityRepository.findById(id).orElse(null));
    }
}
