package com.agrzybow.companyapp.backend.formmanagement.model.mapper;

import com.agrzybow.companyapp.backend.formmanagement.model.FormEntity;
import com.agrzybow.companyapp.backend.formmanagement.model.dto.FormDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface FormEntityDTOMapper {
    List<FormEntity> dtosToEntities(List<FormDTO> formDTOs);
    List<FormDTO> entitiesToDtos(List<FormEntity> formEntities);
    FormDTO entityToDto(FormEntity formEntity);
}
